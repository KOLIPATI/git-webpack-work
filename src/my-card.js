import '../css/my-card.scss'
console.log("its working")

const temp = document.createElement('template')
temp.innerHTML = `
<div class="my-card">
<div class="user-name"><slot name="name"/></div>
<div class="user-info"><slot name="info"/></div>

</div>`

class Mycard extends HTMLElement {
    constructor() {
        super();
        this.attachShadow({ mode: 'open' });
        this.shadowRoot.appendChild(temp.content.cloneNode(true));
    }
}
window.customElements.define("my-card", Mycard);